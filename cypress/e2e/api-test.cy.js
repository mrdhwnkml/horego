// cypress/integration/api.spec.js

describe('API Tests', () => {
    let accessToken,latestId;
    let titleArtcile = 'Updated Article'
     
    it('User login, should be success', () => {
      const apiUrl = 'https://apingweb.com/'; 
  
      cy.request({
        method: 'POST',
        url: apiUrl+'api/login',
        body: {
          email: 'superman@gmail.com',
          password: '123456',
        },
      }).then((response) => {
        // Validasi respons
        expect(response.status).to.equal(200);
        accessToken = response.body.token;
      });
    });

    it('User get all article should be success', () => {
        const apiUrl = 'https://apingweb.com/'; 
    
        cy.request({
          method: 'GET',
          url: apiUrl+'api/articles',
          headers: {
            Authorization: `Bearer ${accessToken}`,
          },
        }).then((response) => {
          // Validasi respons
          expect(response.status).to.equal(200);
          
        });
      });
  
      it('User try accees article without login should be error', () => {
        const apiUrl = 'https://apingweb.com/'; 
    
        cy.request({
          method: 'GET',
          url: apiUrl+'api/articles',
          failOnStatusCode: false, // Allow the request to fail
          
        }).then((response) => {
          // Validasi respons
          expect(response.status).to.equal(404);
          expect(response.body.message).to.equal("API not found or wrong method");
          
        });
      });

      it('User create new article should be success', () => {
        const apiUrl = 'https://apingweb.com/'; 
    
        cy.request({
          method: 'POST',
          url: apiUrl+'api/article/create',
          headers: {
            Authorization: `Bearer ${accessToken}`,
          },
          failOnStatusCode: false, // Allow the request to fail
          body: { 
            "title":"New Article",             
            "body":"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industries standard dummy text ever since the 1500s", 
            "picture":"https://example.com/lorem.png"         
        }
        }).then((response) => {
          // Validasi respons
          expect(response.status).to.equal(200);
        });
      });

      it('User get latest article should be success', () => {
        const apiUrl = 'https://apingweb.com/'; 
    
        cy.request({
          method: 'GET',
          url: apiUrl+'api/articles',
          failOnStatusCode: false, // Allow the request to fail
          headers: {
            Authorization: `Bearer ${accessToken}`,
          },
        }).then((response) => {
          // Validasi respons
          expect(response.status).to.equal(200);
          let ResultArray = response.body.result
          let countArray = ResultArray.length
          let finalArray = countArray-1
          latestId = response.body.result[finalArray].id
        });
      });

      it('User update specific article should be success', () => {
        const apiUrl = 'https://apingweb.com/'; 
    
        cy.request({
          method: 'PUT',
          url: apiUrl+'api/article/edit/'+latestId,
          failOnStatusCode: false, // Allow the request to fail
          headers: {
            Authorization: `Bearer ${accessToken}`,
          },
          body: { 
              "title":titleArtcile,             
              "body":"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industries standard dummy text ever since the 1500s", 
              "picture":"https://example.com/lorem.png"         
          }
          
        }).then((response) => {
          // Validasi respons
          expect(response.status).to.equal(200);
          
        });
      });

      it('User check article is updated', () => {
        const apiUrl = 'https://apingweb.com/'; 
    
        cy.request({
          method: 'GET',
          url: apiUrl+'api/article/'+latestId,
          failOnStatusCode: false, // Allow the request to fail
          headers: {
            Authorization: `Bearer ${accessToken}`,
          },
        }).then((response) => {
          // Validasi respons
          expect(response.status).to.equal(200);
          expect(response.body.result[0].title).to.equal(titleArtcile);
         
          
        });
      });
  
  
  });
  
